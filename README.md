# ありがとうジャバのAPI(deprecated)

これはFirebase でやっていたころの残骸です。
現在はCloud RunでRustのプログラムを動かす感じでやってます：
https://gitlab.com/arigato-java/arijava-api

-----

試しにFirebase で実現してみました

## 設定

APIキーなど秘密情報は、間違えてリポジトリにコミットしないよう、Firebase Functions Config にて管理している。

```
firebase functions:config:get
```

で現在の設定を確認可能。以下のコンフィグを使っている

- arijava.hcaptchasecret
   - hCaptcha のシークレット。 `0x` で始まる
- arijava.cipherkey
   - 暗号化キー. 16進で `1a2b3c...` といった感じの文字列
- arijava.corsorigin
   - CORS で期待されるOrigin. 本番環境では `https://arigato-java.download`

