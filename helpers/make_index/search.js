const fs=require('fs');
const path=require('path');
const lunr = require('lunr');

const query=process.argv[2];

var idx=lunr.Index.load(JSON.parse(fs.readFileSync('lunrindex.json',{encoding:'utf-8'})));
var snippets=JSON.parse(fs.readFileSync('docinfo.json',{encoding:'utf-8'}));
var results=idx.search(query).map((res)=>{
	let val=snippets[res.ref];
	val.url=res.ref;
	return val;
});
var template=fs.readFileSync(path.resolve(__dirname,'results.html'),{encoding:'utf-8'});
let ejs=require('ejs');
var stat=fs.statSync('lunrindex.json');
var html=ejs.render(template,{results:results,query:query,lastUpdate:stat.mtime});

console.log(html);
