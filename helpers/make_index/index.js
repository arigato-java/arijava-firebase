const path=require('path');
const fs=require('fs');

var webdriver=require('selenium-webdriver'),
	By = webdriver.By,
	until = webdriver.until;
var firefox=require('selenium-webdriver/firefox');

const lunr = require('lunr');
require('lunr-languages/lunr.stemmer.support.js')(lunr);
require('lunr-languages/tinyseg.js')(lunr);
require('lunr-languages/lunr.ja.js')(lunr);
//require('lunr-languages/lunr.multi.js')(lunr);

// Selenium driver
var driver;
// Lunr index
var idx;
// Dictionary for snippets
var documentinfo={};
// Snippet length in number of characters
const SNIPPET_LENGTH=40;

function prepareBrowser(){
	var options = new firefox.Options();
	options.addArguments('-headless -P testbench');
	return new webdriver.Builder()
		.forBrowser('firefox')
		.setFirefoxOptions(options)
		.build();
}

function quitBrowser(){
	return driver.quit();
}

function shouldBlockFile(query_url,query_path){
	if(query_url.startsWith('deploy/') ||
		query_url==='robots.txt' ||
		query_url.startsWith('.')
	) { return true; }
	return false;
}

function indexFileHTML(query_url,query_path,addfn){
	// remove index.html from URL (referencing just the directory is preferred)
	if(query_url.endsWith('/index.html') || query_url === 'index.html'){
		query_url=query_url.substring(0,(query_url.length)-('index.html'.length));
	}
	return driver.get('file://'+query_path)
	.then(()=>{
		return driver.findElement(By.tagName('body')).getAttribute('innerText');
	})
	.then((plaintext)=>{
		const secondline=1+plaintext.indexOf("\n");
		// 検索結果に表示したい情報
		documentinfo[query_url]={
			title:plaintext.substring(0,secondline-1).trim(),
			snippet:plaintext.substring(secondline,secondline+SNIPPET_LENGTH).trim()
		};
		const doc={
			'ref':query_url,
			'body':plaintext
		};
		console.log(JSON.stringify(doc));
		addfn(doc);
	});
}

function indexFileText(query_url,query_path,addfn){
	return new Promise((resolve,reject)=>{resolve();});
}

function indexFile(query_url,query_path,addfn){
	var nextFn;
	if(query_url.endsWith('.html') ||
		query_url.endsWith('.htm')){
		nextFn=indexFileHTML;
	} else if(query_url.endsWith('.txt')){
		nextFn=indexFileText;
	} else {
		return false;
	}
	return nextFn(query_url,query_path,addfn);
}

function handleDir(urlprefix,path_to_dir,addfn){
	return new Promise((resolve,reject)=>{
		fs.readdir(path_to_dir,{withFileTypes:true},(err,files)=>{
			if(err) {
				reject(err);
				return;
			}
			var jobs=[];
			files.forEach((entry)=>{
				var url_appended=path.posix.join(urlprefix,entry.name);
				var path_appended=path.join(path_to_dir,entry.name);
				if(shouldBlockFile(url_appended,path_appended)) {
					return;
				} else if(entry.isDirectory()){
					jobs.push(handleDir.bind(null,url_appended,path_appended,addfn));
				} else if(entry.isFile()) {
					jobs.push(indexFile.bind(null,url_appended,path_appended,addfn));
				}
			});
			return jobs.reduce((accumulator,currentValue)=>accumulator.then(currentValue),
				new Promise((resolve,reject)=>{resolve();})
			).then(resolve);
		});
	});
}

(async function(){
	await prepareBrowser()
		.then((InDriver)=>{
			driver=InDriver;
			return new Promise(async (resolve,reject)=>{
				var builder = new lunr.Builder;
				builder.pipeline.add(
					lunr.trimmer,
					lunr.stopWordFilter,
					lunr.stemmer
				);
				builder.searchPipeline.add(
					lunr.stemmer
				);
				builder.ref('ref');
				builder.field('body');
				//builder.use(lunr.multiLanguage('en','jp'));
				builder.use(lunr.jp);
				
				await handleDir('',process.argv[2],builder.add.bind(builder));
				var index=builder.build();
				resolve(index);
			});
		})
		.then((index)=>{
			idx=index;
			return quitBrowser();
		})
		.then(()=>{
			fs.writeFileSync('lunrindex.json',JSON.stringify(idx.toJSON()));
			fs.writeFileSync('docinfo.json',JSON.stringify(documentinfo));
		})
		.catch((err)=>{
			console.error(err.toString());
		});
})();
