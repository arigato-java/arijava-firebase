const functions = require('firebase-functions');
const express = require('express');
const crypto=require('crypto');
const app = express();
// [END import]

const corsorigin=functions.config().arijava.corsorigin;

// [START middleware]
const cors = require('cors');
const corsOptions = {
	origin: function(origin,callback){
		if(origin===corsorigin){callback(null,true);}
		else{callback(new Error('Not allowed by CORS'));}
	},
	methods: ['GET', 'POST'],
	allowedHeaders: ['Content-Type', 'Authorization'],
};
app.use(express.json()) // for parsing application/json
// [END middleware]

const url_prefix='/v1';

// [START api]
// This endpoint is the BONG API. It returns the bongs as an API.
app.options(url_prefix+'/hello', cors(corsOptions));
app.get(url_prefix+'/hello', cors(corsOptions), (req, res) => {
	app.set('json spaces', 2);
  // [START_EXCLUDE silent]
  // [START cache]
  res.set('Cache-Control', 'no-cache,no-store');
  // [END cache]
  // [END_EXCLUDE silent]
  res.json({
	  headers: req.headers,
	  ip: req.ip,
	  ips: req.ips
	});
});
// [END api]
const encryptedEncoding='base64';
const plaintextEncoding='utf8';
const keyEncoding='hex';
const algorithm='aes-256-cbc';

function decode_message(ciphertext,keytext,ivtext){
	const key=Buffer.from(keytext,keyEncoding);
	const iv=Buffer.from(ivtext,keyEncoding);
	const decipher=crypto.createDecipheriv(algorithm,key,iv);

	let decrypted=decipher.update(ciphertext,encryptedEncoding,plaintextEncoding);
	decrypted+=decipher.final(plaintextEncoding);

	return decrypted;
}

async function turnstileValidate(secret,token){
	let fd=new FormData();
	fd.append('secret', secret);
	fd.append('response', token);
	let response = await fetch(
		'https://challenges.cloudflare.com/turnstile/v0/siteverify',
		{
			method: 'POST',
			mode: 'no-cors',
			body: fd
		});
	if(response.ok) {
		let response_json = await response.json();
		return response_json.success;
	} else {
		return false;
	}
}

app.options(url_prefix+'/toiawase', cors(corsOptions));
app.post(url_prefix+'/toiawase', cors(corsOptions), async (req, res) => {
  // [START cache]
  res.set('Cache-Control', 'no-cache,no-store');
  // [END cache]

	const fn_config=functions.config();
	const key=fn_config.arijava.cipherkey;
	const turnstilesecret=fn_config.arijava.turnstilesecret;
	let url=decode_message(req.body.destination,key,req.body.iv);
	try {
		let captcha_res=await turnstileValidate(turnstilesecret,req.body.captcha)
		if(!captcha_res){
			throw "Captcha validation failed."
		}
		let postres = await fetch(url,{
			method: 'POST',
			headers: {
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				embeds:[{type:'rich',description:req.body.message,color:15601248}]
			})
		});
		if (postres.ok) {
			res.json({success:true});
		} else{
			throw "Discord post failed";
		}
	}catch(err){
		console.log(err);
		res.json({success:false});
	}
});

const fs=require('fs');
const path=require('path');
let lunr;
let ejs;
let idx;
let snippets;
let template;
// search
app.get(url_prefix+'/search', (req, res) => {
	// [START cache]
	res.set('Cache-Control', 'public,max-age=86400');
	res.set('Content-Type', 'text/html;charset=utf-8');
	res.set('Content-Security-Policy', "default-src 'self'; script-src 'none'");
	// [END cache]
	lunr = lunr || require('lunr');
	const indexpath=path.resolve(__dirname,'lunrindex.json');

	idx=idx || lunr.Index.load(JSON.parse(fs.readFileSync(indexpath,{encoding:'utf-8'})));
	snippets=snippets || JSON.parse(fs.readFileSync(path.resolve(__dirname,'docinfo.json'),{encoding:'utf-8'}));
	var searchResult=idx.search(req.query.q).map((res)=>{
		let val=snippets[res.ref];
		val.url=res.ref;
		return val;
	});

	template=template || fs.readFileSync(path.resolve(__dirname,'results.html'),{encoding:'utf-8'});
	ejs=ejs || require('ejs');
	var html=ejs.render(template,{
		results:searchResult,
		query:req.query.q
	});
	
	res.send(html);
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// [START export]
// Export the express app as an HTTP Cloud Function
exports.app = functions.https.onRequest(app);
// [END export]
